#!/usr/bin/env python

import os
import sys

sys.path.extend(['/usr/local/django', '/usr/local/django/site_accueil_crans'])
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
