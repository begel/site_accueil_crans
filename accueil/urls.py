# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^login/$', auth_views.login,
    {'template_name': 'accueil/login.html', 'redirect_field_name': 'search'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'search'}, name='logout'),
    url(r'^$', views.mainView, name="main"),
    url(r'^search/$', views.searchView, name='search'),
    url(r'^added/$', views.addFavView, name='addFav'),
    url(r'^deleted/$', views.delFavView, name='delFav'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
]
