# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Website, Favorite, CransSite
# Register your models here.
@admin.register(Website)
class WebsiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'category')
    list_filter = ['category']
    search_fields = ['name']
    ordering = ['category','name']

    # On administration, we only see general informations
    # not people's favorites
    def get_queryset(self, request):
        qs = super(WebsiteAdmin, self).get_queryset(request)
        return qs.exclude(category='favorite')

admin.site.register(CransSite)
