# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible

categoryChoices = (
    ('crans', 'Crans'),
    ('favorite', 'Favorite'),
    ('searchEngine', 'SearchEngine')
)

# Create your models here.
@python_2_unicode_compatible
class Website(models.Model):
    url = models.URLField()
    name = models.CharField(max_length=50)
    category = models.CharField(choices=categoryChoices, max_length=12)

    def __str__(self):
        return self.name

class Favorite(Website):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

class CransSite(Website):
    description = models.CharField(max_length=100)
    need_account = models.BooleanField(default=False)
