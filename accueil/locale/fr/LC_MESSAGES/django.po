# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-08 16:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: templates/accueil/accueil.html:3
msgid "Accueil"
msgstr ""

#: templates/accueil/accueil.html:7
msgid "Se déconnecter"
msgstr ""

#: templates/accueil/accueil.html:11 templates/accueil/login.html:35
msgid "Se connecter"
msgstr ""

#: templates/accueil/accueil.html:16
msgid "Page d'accueil"
msgstr ""

#: templates/accueil/accueil.html:38
msgid "Sites Crans"
msgstr ""

#: templates/accueil/accueil.html:47
msgid "sites nécessitant un compte Crans"
msgstr ""

#: templates/accueil/accueil.html:56
msgid "Mes favoris"
msgstr ""

#: templates/accueil/accueil.html:72
msgid "News Crans"
msgstr ""

#: templates/accueil/base.html:46
msgid "Contacter le Crans"
msgstr ""

#: templates/accueil/base.html:48
msgid "Horaires des permanences"
msgstr ""

#: templates/accueil/gestion_favoris.html:4
msgid "Ajout"
msgstr ""

#: templates/accueil/gestion_favoris.html:14
msgid "Ajouter"
msgstr ""

#: templates/accueil/gestion_favoris.html:18
msgid "Connecte-toi pour ajouter des favoris"
msgstr ""

#: templates/accueil/gestion_favoris.html:24
msgid "Suppression"
msgstr ""

#: templates/accueil/gestion_favoris.html:30
msgid "Supprimer"
msgstr ""

#: templates/accueil/login.html:4
msgid "Connexion - Accueil crans"
msgstr ""

#: templates/accueil/login.html:7
msgid "Retour"
msgstr ""

#: templates/accueil/login.html:10
msgid "Connexion à la page d'accueil"
msgstr ""

#: templates/accueil/login.html:11
msgid "La connexion te permet d'ajouter et sauvegarder tes favoris"
msgstr ""

#: templates/accueil/login.html:26
msgid "Pour te connecter, utilise tes identifiants Crans."
msgstr ""

#: templates/accueil/login.html:33
msgid "Tes identifiants n'ont pas permis de t'identifier. Recommence."
msgstr ""
