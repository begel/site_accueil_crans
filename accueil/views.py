# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from urllib import quote_plus
from django.contrib.auth.models import User

from .models import Website, Favorite, CransSite
from .forms import SearchForm, AddFavForm, DelFavForm

# Create your views here.
def mainView(request, addform=AddFavForm(), delform=None):
    if request.user.is_authenticated():
        favorites = request.user.favorite_set.all()
        is_connected = True
        if delform==None:
            choices = [(f.pk,f.name) for f in favorites]
            delform = DelFavForm(choices=choices)
    else:
        favorites = []
        is_connected = False
    cranssites = CransSite.objects.all()
    form = SearchForm()
    context = {'cranssites':cranssites, 'form':form, 'delform': delform,
    'addform':addform, 'favorites': favorites, 'is_connected':is_connected}
    return render(request, 'accueil/accueil.html', context)

def addFavView(request):
    if request.method == 'POST':
        addform = AddFavForm(request.POST)
        if addform.is_valid():
            addurl = addform.cleaned_data['url']
            addname = addform.cleaned_data['name']
            newfav = Favorite(name=addname,url=addurl,category='favorite',owner=request.user)
            newfav.save()
    else:
        addform = AddFavForm()
    return mainView(request,addform=addform)

def delFavView(request):
    if request.method =='POST':
        favorites = request.user.favorite_set.all()
        choices = [(f.pk,f.name) for f in favorites]
        delform = DelFavForm(request.POST,choices=choices)
        if delform.is_valid():
            favorites = delform.cleaned_data['favorites']
            for fav in favorites:
                Favorite.objects.get(pk=fav).delete()
    else:
        delform = None
    return mainView(request, delform=delform)

def searchView(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            searchEngine = form.cleaned_data['searchEngine']
            searchWord = form.cleaned_data['searchWord']
            url = searchEngine+quote_plus(searchWord.encode("utf-8"), safe='/')
            return redirect(url)
    else:
        return mainView(request)
