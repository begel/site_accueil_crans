# -*- coding: utf-8 -*-
from django import forms
from .models import Website

class SearchForm(forms.Form):
    searchWord = forms.CharField(required=False)
    searchEngine = forms.ChoiceField()
    def __init__(self, *args, **kwargs):
        searchEngines = Website.objects.filter(category='searchEngine')
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['searchEngine'].choices = [(s.url, s.name) for s in searchEngines]

class AddFavForm(forms.Form):
    name = forms.CharField(max_length=30)
    url = forms.URLField(initial='http://')

class DelFavForm(forms.Form):
    favorites = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)
    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices')
        super(DelFavForm, self).__init__(*args, **kwargs)
        self.fields['favorites'].choices = choices
